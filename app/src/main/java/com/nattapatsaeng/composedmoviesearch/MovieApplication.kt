package com.nattapatsaeng.composedmoviesearch
import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MovieApplication : Application()