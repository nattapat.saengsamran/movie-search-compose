package com.nattapatsaeng.composedmoviesearch.domain.usecases.getmovies

import com.nattapatsaeng.composedmoviesearch.common.Resource
import com.nattapatsaeng.composedmoviesearch.data.remote.dto.toMovie
import com.nattapatsaeng.composedmoviesearch.domain.model.Movie
import com.nattapatsaeng.composedmoviesearch.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(
    private val repository: MovieRepository
) {
    operator fun invoke(query: String, page: Int): Flow<Resource<List<Movie>>> = flow {
        try {
            emit(Resource.Loading())
            val movies: List<Movie> = repository.getMovies(query, page).map { it.toMovie() }
            emit(Resource.Success(movies))
        }
        catch (e: HttpException) {
            emit(Resource.Error(e.message?: "Unknown error occur"))
        }
        catch (e: IOException) {
            emit(Resource.Error("Couldn't reach server. Check your internet connection"))
        }
    }
}