package com.nattapatsaeng.composedmoviesearch.domain.usecases.getmovie

import com.nattapatsaeng.composedmoviesearch.common.Resource
import com.nattapatsaeng.composedmoviesearch.data.remote.dto.toMovie
import com.nattapatsaeng.composedmoviesearch.domain.model.Movie
import com.nattapatsaeng.composedmoviesearch.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(
    private val repository: MovieRepository,
) {
    operator fun invoke(query: String, id: Int): Flow<Resource<Movie>> = flow {
        try {
            emit(Resource.Loading())
            val movie: Movie = repository
                .getMovies(query, 1)
                .map { it.toMovie() }
                .first { it.id == id }
            emit(Resource.Success(movie))
        }
        catch (e: HttpException) {
            emit(Resource.Error(e.message?: "Unknown error occur"))
        }
        catch (e: IOException) {
            emit(Resource.Error("Couldn't reach server. Check your internet connection"))
        }
    }
}