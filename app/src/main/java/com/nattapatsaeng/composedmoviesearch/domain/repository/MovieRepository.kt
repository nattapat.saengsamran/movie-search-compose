package com.nattapatsaeng.composedmoviesearch.domain.repository

import com.nattapatsaeng.composedmoviesearch.data.remote.dto.MovieDto

interface MovieRepository {
    suspend fun getMovies(query: String ,page: Int): List<MovieDto>
}