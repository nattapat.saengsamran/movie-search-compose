package com.nattapatsaeng.composedmoviesearch.data.remote

import com.nattapatsaeng.composedmoviesearch.common.Constants
import com.nattapatsaeng.composedmoviesearch.data.remote.dto.MoviesDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieApi {
    @Headers(Constants.API_KEY)
    @GET("/api/movies/search")
    suspend fun getMovie( @Query("query") queryWord: String, @Query("page") page: Int): MoviesDto
}