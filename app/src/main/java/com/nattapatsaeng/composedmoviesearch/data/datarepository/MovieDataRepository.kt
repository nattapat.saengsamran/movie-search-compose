package com.nattapatsaeng.composedmoviesearch.data.datarepository

import com.nattapatsaeng.composedmoviesearch.data.remote.MovieApi
import com.nattapatsaeng.composedmoviesearch.data.remote.dto.MovieDto
import com.nattapatsaeng.composedmoviesearch.domain.repository.MovieRepository
import javax.inject.Inject

class MovieDataRepository @Inject constructor(
    private val api: MovieApi
) : MovieRepository {
    override suspend fun getMovies(query: String, page: Int): List<MovieDto> {
        return api.getMovie(query, page).results
    }
}