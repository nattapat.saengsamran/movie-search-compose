package com.nattapatsaeng.composedmoviesearch.common

object Constants {
    const val API_KEY = "api-key: 623e6b5af5614d7debb532e0472b85c2997e5400"
    const val BASE_URL = "http://scb-movies-api.herokuapp.com/"

    const val PARAM_QUERY = "movieQuery"
    const val PARAM_PAGE = "moviePage"
}